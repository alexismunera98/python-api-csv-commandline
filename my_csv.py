import csv
import requests

r =  requests.get("https://swapi.co/api/vehicles/")

response = r.json()
results = response['results']
columns = results[0].keys()

with open("vehicles.csv", "w") as file:
    writer = csv.DictWriter(file, fieldnames=columns)
    writer.writeheader()
    writer.writerows(results)
